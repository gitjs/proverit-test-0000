
const ИМЯ = "Проверить 0000";




// // // //


УведомитьОбОшибкеСохранения = function(мир)
{
    document.body.innerHTML += `

<div class="элемент">
    <p>ОШИБКА Не удалось сохранить модуль</p>
</div>

    `;

};


// // // //


ПроверитьСохранение = function(мир)
{
    var модуль = мир.модули.модульПоИмени(ИМЯ);
    // Добавить файл.
    модуль.структура.push({
        "имя": "/сохранено",
    });
    модуль.содержимое["/сохранено"] = "Новое содержимое: " + new Date();

    мир.модули.сохранитьСодержимое([модуль.указатель]);
    мир.модули.сохранилиСодержимое.подписатьРаз(function() {
        мир.уведомить("сохранили");
    });
};


// // // //


УведомитьОбОшибкеЗагрузкиСодержимого = function(мир)
{
    document.body.innerHTML += `

<div class="элемент">
    <p>ОШИБКА Не удалось загрузить содержимое модулей</p>
</div>

    `;

};


// // // //


ЗагрузитьСодержимоеМодулей = function(мир)
{
    var указатели = мир.модули.указатели();
    мир.модули.загрузитьСодержимое(указатели);
    мир.модули.загрузилиСодержимое.подписатьРаз(function() {
        мир.уведомить("загрузили содержимое модулей");
    });
    мир.модули.неЗагрузилиСодержимое.подписатьРаз(function() {
        мир.уведомить("не загрузили содержимое модулей");
    });
};


// // // //


ПроверитьМодулиСодержимое = function(мир)
{
    var указатели = мир.модули.указатели();
    var html = "";
    for (var номер in указатели)
    {
        var указатель = указатели[номер];
        console.log("указатель", указатель);
        var описание = мир.модули.описание(указатель);
        var htmlФайлы = "";
        for (var id in описание.структура)
        {
            var файл = описание.структура[id];
            var содержимое = мир.модули.содержимое(указатель, файл.имя);
            var начало = "(null)";
            if (!содержимое)
            {
                содержимое = "";
            }
            else
            {
                начало = содержимое.substring(0, 50);
            }
            htmlФайлы += `

<li>${файл.имя} (${содержимое.length}): <pre>${начало}</pre></li>

            `;
        }
        html += `<li>${указатель}<p><ul>${htmlФайлы}</ul></p></li>`;
    }
    document.body.innerHTML += `

<div class="элемент">
    <p>Вызов для каждого файла имеющегося указателя: <pre>Модули.содержимое(указатель, файл)</pre></p>
    <p>Ответ: <ol>${html}</ol></p>
</div>

    `;
};


// // // //


ПроверитьМодулиОписание = function(мир)
{
    var указатели = мир.модули.указатели();
    var html = "";
    for (var номер in указатели)
    {
        var указатель = указатели[номер];
        var описание = JSON.stringify(мир.модули.описание(указатель), null, 4);
        html += `

<li><pre>${описание}</pre></li>

        `;
    }
    document.body.innerHTML += `

<div class="элемент">
    <p>Вызов для каждого имеющегося указателя: <pre>Модули.описание(указатель)</pre></p>
    <p>Ответ: <ol>${html}</ol></p>
</div>

    `;
};


// // // //


ЗадатьВидЭлементов = function()
{
    var css = `

.элемент
{
    margin: 0.5em;
    padding: 0.5em;
    border: 1px solid lightgrey;
    border-radius: 5px;
}

    `;
    var вид = document.createElement("style");
    вид.innerHTML = css;
    document.head.appendChild(вид);
};


// // // //


ПроверитьМодулиУказатели = function(мир)
{
    var указатели = мир.модули.указатели();
    var html = "";
    for (var номер in указатели)
    {
        var указатель = указатели[номер];
        html += `

<li><pre>${указатель}</pre></li>

        `;
    }
    document.body.innerHTML += `

<div class="элемент">
    <p>Вызов: <pre>Модули.указатели()</pre></p>
    <p>Ответ: <ol>${html}</ol></p>
</div>

    `;
};
